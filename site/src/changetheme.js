//Full document © 2019 Nathan B (https://nate.iunctiozero.com)

function swapTheme() {
    var body = document.getElementById('body')
    var colorSquare = document.getElementById('colorSquare')
    var invert = document.getElementById('invert')

    if (body.style.color == "black") {
        body.style.backgroundColor = "black"
        body.style.color = "whitesmoke"
        colorSquare.style.backgroundColor = "white"
        invert.style.filter = "invert(1)"
    } else if (body.style.color == "whitesmoke" || body.style.color == "") {
        body.style.backgroundColor = "whitesmoke"
        body.style.color = "black" 
        colorSquare.style.backgroundColor = "black"
        invert.style.filter = "none"
    } else {
        console.log('Error: No colors')
    }

}